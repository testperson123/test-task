# Project API Automation for Todo.ly - test task

The test task for Project API test automation. This manual was tested on MacOS and most likely will work on MacOS and Linux.

## Prerequisites

Python and `pipenv` are required to be installed globally:

- [Python v.3.9](https://www.python.org/downloads/)
- [pipenv](https://github.com/pypa/pipenv)

## Installation

Navigate to test project root:
```
cd test_task
```
Install virtual environment:
```
pipenv install
```

## Environment setup

Create an environment file out of template, fill in (email and password) and source it:

```
cp env.sh.sample env.sh
. ./env.sh
```

## Running tests

```
pipenv run python3 -m pytest api_tests/ --disable-warnings --verbose --html=test-report.html --self-contained-html
```

This will discover and execute all the functional tests for API. In order to run specific test file, use

```
pipenv run python3 -m pytest api_tests/negative/auth_test.tavern.yaml --disable-warnings --verbose --html=test-report.html --self-contained-html
```

Also the tests can be structured within folder (e.g. test suite). You can recursively run the tests on each folder lever, e.g.:

```
pipenv run python3 -m pytest api_tests/negative/ --disable-warnings --verbose --html=test-report.html --self-contained-html
```

After the tests are done, *test-report.html* file is created in the project's root folder. It will contain the summary of the test run.