import unittest
from schema import Schema, And, Or, Regex

tc = unittest.TestCase()

project_response_schema = {
    "Id": int,
    "Content": And(str, len),
    "ItemsCount": int,
    "Icon": int,
    "ItemType": int,
    "ParentId": None,
    "Collapsed": bool,
    "ItemOrder": int,
    "Children": list,
    "IsProjectShared": bool,
    "ProjectShareOwnerName": None,
    "ProjectShareOwnerEmail": None,
    "IsShareApproved": bool,
    "IsOwnProject": bool,
    "LastSyncedDateTime": Regex("/.*Date.*"),
    "LastUpdatedDate": Regex("/.*Date.*"),
    "Deleted": bool,
    "SyncClientCreationId": None
}
item_schema = {
    "Id": int,
    "Content": And(str, len),
    "ItemType": int,
    "Checked": bool,
    "ProjectId": int,
    "ParentId": None,
    "Path": str,
    "Collapsed": bool,
    "DateString": None,
    "DateStringPriority": int,
    "DueDate": "",
    "Recurrence": None,
    "ItemOrder": int,
    "Priority": int,
    "LastSyncedDateTime": str,
    "Children": list,
    "DueDateTime": None,
    "CreatedDate": str,
    "LastCheckedDate": Or(Regex("/.*Date.*"), None),
    "LastUpdatedDate": Regex("/.*Date.*"),
    "Deleted": bool,
    "Notes": "",
    "InHistory": bool,
    "SyncClientCreationId": None,
    "DueTimeSpecified": bool,
    "OwnerId": int
}


def check_project_response(content):
    schema = Schema(project_response_schema)
    schema.validate(content)


def check_created_project_response(response, name_is):
    content = response.json()
    check_project_response(content)
    tc.assertEqual(content["Content"], name_is)


def check_deleted_project_response(response):
    content = response.json()
    check_project_response(content)
    tc.assertTrue(content["Deleted"])


def check_get_all_projects_response(response, name_is):
    content = response.json()
    schema = Schema(
        [
            project_response_schema
        ]
    )
    schema.validate(content)
    names = [x["Content"] for x in content]
    tc.assertEqual(len(names), 1)  # only newly created
    # comparing the actual set of names with expected one
    tc.assertEqual(set(names), set((name_is,)))


def check_created_item(response):
    content = response.json()
    schema = Schema(item_schema)
    schema.validate(content)


def check_project_items(response, item_name):
    content = response.json()
    schema = Schema(
        [
            item_schema
        ]
    )
    schema.validate(content)
    tc.assertEqual(content[0]["Content"], item_name)
